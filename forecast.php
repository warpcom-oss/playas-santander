<?php

$user = "sc_santander";
$pass = "<password>";
$host = "localhost";
$port = "5432";
$db   = "sc_santander";

$schema = 'sc_santander';
$table_forecast = 'beaches_seaforecast_lastdata';

$fields_forecast = "
entitytype,
entityid,
dataprovider,
to_char(dateobserved AT TIME ZONE 'UTC', 'YYYY-MM-DD\"T\"HH24:MI:SS\"Z\"') as timeinstant_rfc,
description,
maximumtemperature,
sky1,
sky2,
swell1,
swell2,
thermalsensation,
uvmax,
watertemperature,
wind1,
wind2
";

function connect($usuario, $pass, $host, $port, $bd)
{
    try {
        $conexion = new PDO(
            "pgsql:user=" . $usuario . ";" .
            "password=" . $pass . ";" .
            "host=" . $host . ";" .
            "port=" . $port . ";" . 
            "dbname=" . $bd
        );
        // Workaround de un bug en pgpool 4.2.2
        // Ver https://github.com/MagicStack/asyncpg/issues/573
        $conexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conexion;
    } catch(PDOException $e) {
        error_log("Error al conectar: " . $e->getMessage());
    }
    // Solo llegamos aqui en caso de error
    return null;
}

function close($connection)
{
    $connection = null;
}

function execute_sql($connection, $sql, $params)
{
    $dbdata = array();
    try {
        $stmt = $connection->prepare($sql);
        if (!is_null($params)) {
            // Contamos con que el array esté numerado desde cero!
            foreach ($params as $index => $value) {
                $stmt->bindParam($index + 1, $value);
            }
        }
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $stmt->execute();
        try {
            while ($row = $stmt->fetch()) {
                $dbdata[] = $row;
            }
        }
        finally {
            $stmt->closeCursor();
        }
    } catch(PDOException $e) {
        error_log("Error ejecutando query " . $sql . ": " . $e->getMessage());
    }
    return $dbdata;
}

function get_entity($connection, $schema, $table, $fields, $id)
{
    $sql = "SELECT " . $fields . " FROM " . $schema . "." . $table;
    $params = array();
    if (!is_null($id)) {
        $sql = $sql . " WHERE entityid = ?";
        $params[] = $id;
    }
    return execute_sql($connection, $sql, $params);
}

if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && 
    strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= strtotime('-2 minutes')) {

    header('HTTP/1.0 304 Not Modified');
    header("Cache-Control: max-age=120, public");
    header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (2 * 60))); // 4 minutes 
    header("Pragma: cache");
    exit;
}

$connection = connect($user, $pass, $host, $port, $db);
$forecast = array();

if (!is_null($connection)) {
    try {
        $id = isset($_GET["id"]) ? $_GET["id"] : null;
        $forecast = get_entity($connection, $schema, $table_forecast, $fields_forecast, $id);
    }
    finally {
        close($connection);
    }
}

header("Cache-Control: max-age=120, public");
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (2 * 60))); // 4 minutes 
header("Pragma: cache");
echo(json_encode($forecast, JSON_NUMERIC_CHECK));

?>
