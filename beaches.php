<?php

$user = "sc_santander";
$pass = "<password>";
$host = "localhost";
$port = "5432";
$db   = "sc_santander";

$schema = 'sc_santander';
$table_beaches = 'beaches_beach_public_lastdata';
$table_poi = 'beaches_poi_public_lastdata';
$table_access = 'beaches_access_public_lastdata';
$table_occupancy = 'beaches_occupancy_public_lastdata';

$fields_beaches = "
entitytype,
entityid,
timeinstant,
timeinstant_rfc,
status,
statusdescription,
flag,
waterquality,
name,
address,
location_json AS location,
areaserved,
width,
length,
beachtype,
whitesand,
isolated,
calmwaters,
blueflag,
q_quality,
strongwaves,
windy,
blacksand,
smokefree,
privatevehicle,
onfoot,
publictransport,
jellyfishwarn,
emas,
coastalprotection,
bathingseason,
occupationrate,
capacity,
contingencyplanversion,
fragmentationtype,
occupancycontroltype,
firstlinebeach,
totalhammocks,
totalumbrellas,
totalawnings,
source,
image,
staysurface,
itinerarysurface,
walkingsurface,
occupancy,
percentageofoccupation,
thresholdwarning,
thresholdcritical
";

$fields_poi = "
entitytype,
entityid,
timeinstant,
source,
timeinstant_rfc,
name,
status,
location_json AS location
";

$fields_access = "
entitytype,
entityid,
timeinstant,
source,
timeinstant_rfc,
name,
description,
category,
status,
width,
location_json AS location,
period,
numberOfIncoming,
numberOfOutgoing
";

function connect($usuario, $pass, $host, $port, $bd)
{
    try {
        $conexion = new PDO(
            "pgsql:user=" . $usuario . ";" .
            "password=" . $pass . ";" .
            "host=" . $host . ";" .
            "port=" . $port . ";" . 
            "dbname=" . $bd
        );
        // Workaround de un bug en pgpool 4.2.2
        // Ver https://github.com/MagicStack/asyncpg/issues/573
        $conexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conexion;
    } catch(PDOException $e) {
        error_log("Error al conectar: " . $e->getMessage());
    }
    // Solo llegamos aqui en caso de error
    return null;
}

function close($connection)
{
    $connection = null;
}

function execute_sql($connection, $sql, $params)
{
    $dbdata = array();
    try {
        $stmt = $connection->prepare($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        if (is_null($params)) {
       	    $stmt->execute();
        } else {
       	    $stmt->execute($params);
        }
        try {
            while ($row = $stmt->fetch()) {
                $dbdata[] = $row;
            }
        }
        finally {
            $stmt->closeCursor();
        }
    } catch(PDOException $e) {
        error_log("Error ejecutando query " . $sql . ": " . $e->getMessage());
    }
    return $dbdata;
}

function get_dependency($connection, $schema, $table, $fields, $beach)
{
    $sql = "SELECT " . $fields . " FROM " . $schema . "." . $table;
    $params = array();
    if (!is_null($beach)) {
        $sql = $sql . " WHERE source = ?";
        $params[] = $beach;
    }
    return execute_sql($connection, $sql, $params);
}

function get_poi($connection, $schema, $table, $fields, $poi, $beach)
{
    $sql = "SELECT " . $fields . " FROM " . $schema . "." . $table . " WHERE entitytype = ?";
    $params = array($poi);
    if (!is_null($beach)) {
        $sql = $sql . " AND source = ?";
        $params[] = $beach;
    }
    return execute_sql($connection, $sql, $params);
}

function get_entity($connection, $schema, $table, $fields, $id)
{
    $sql = "SELECT " . $fields . " FROM " . $schema . "." . $table;
    $params = array();
    if (!is_null($id)) {
        $sql = $sql . " WHERE entityid = ?";
        $params[] = $id;
    }
    return execute_sql($connection, $sql, $params);
}

function filter_entities($entities, $id)
{
    $result = array();
    for ($i = 0; $i < count($entities); ++$i) {
        $source = $entities[$i]->source;
        if (strcmp($source, $id) == 0) {
            //if (property_exists($entities[$i], "location")) {
            //    $entities[$i]->location = json_decode($entities[$i]->location);
            //}
            $result[] = $entities[$i];
        }
    }
    return $result;
}

if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && 
    strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= strtotime('-2 minutes')) {

    header('HTTP/1.0 304 Not Modified');
    header("Cache-Control: max-age=120, public");
    header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (4* 60))); // 4 minutes 
    header("Pragma: cache");
    exit;
}

$connection = connect($user, $pass, $host, $port, $db);
$beaches = array();

if (!is_null($connection)) {
    try {
        $id = isset($_GET["beach"]) ? $_GET["beach"] : null;
        $beaches = get_entity($connection, $schema, $table_beaches, $fields_beaches, $id);

        // Hago la query aqui, fuera del bucle, en lugar de una query por playa,
        // para optimizar la comunicacion con base de datos.
        $flags = get_poi($connection, $schema, $table_poi, $fields_poi, 'POI_lifeGuardPost', $id);
        $signs = get_poi($connection, $schema, $table_poi, $fields_poi, 'POI_informationSign', $id);
        $access = get_dependency($connection, $schema, $table_access, $fields_access, $id);

        for ($i = 0; $i < count($beaches); ++$i) {
            //if (property_exists($beaches[$i], "location")) {
            //    $beaches[$i]->location = json_decode($beaches[$i]->location);
            //}
            /*$occupancy = get_dependency($connection, $schema, $table_occupancy, $fields_occupancy, $beaches[$i]->entityid);
            if ($occupancy) {
                $beaches[$i]->join_occupancy = $occupancy[0];
            }*/
            $beaches[$i]->join_flags  = filter_entities($flags, $beaches[$i]->entityid);
            $beaches[$i]->join_signs  = filter_entities($signs, $beaches[$i]->entityid);
            $beaches[$i]->join_access = filter_entities($access, $beaches[$i]->entityid);
        }
    }
    finally {
        close($connection);
    }
}

header("Cache-Control: max-age=120, public");
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (2 * 60))); // 4 minutes 
header("Pragma: cache");
echo(json_encode($beaches, JSON_NUMERIC_CHECK));

?>
