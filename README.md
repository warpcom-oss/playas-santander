# Configuración de API y Drupal

## Pool de conexiones

La API accede a postgres a través de un pool de conexiones implementado por **pgBouncer**. La configuración de pgBouncer se almacena en [/etc/pgbouncer/pgbouncer.ini](doc/pgbouncer.ini), y está configurado para permitir conexiones desde cualquier usuario local, sin pedir password. Los passwords se almacenan en [/etc/pgbouncer/userlist.txt](doc/userlist.txt).

## Drupal

La configutación de Drupal se almacena en `/var/www/drupal/web/sites/default/settings.php`, e incluye las conexiones a base de datos.

## Scripts

Los scripts se guardan en `/var/www/drupal/web/api`.
